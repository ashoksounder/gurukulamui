import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators, FormArray } from '@angular/forms';
import { AdminFormsService } from '../../../services/admin-forms.service';

@Component({
  selector: 'app-create-class',
  templateUrl: './create-class.component.html',
  styleUrls: ['./create-class.component.scss']
})
export class CreateClassComponent implements OnInit {

  constructor(private fb: FormBuilder, private adminForm: AdminFormsService) { }
  
  classForm:FormGroup;
  submitted:boolean = false;
  subjects: any[] = [];
  teachers: any[] = [];
  standards : any[] = [];
  message: string;
  messageFlag:boolean = false;
  previewFlag: boolean = false;
  emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  phonePattern = /^[6789]\d{9}$/;

  class = {
      groups: [
        {
          groupName: "",
          subjects:[
            {
              subject: "",
              teacher:"",
            }
          ]
        }
      ]
    };

  standardConfig = {
    labelField: 'std_name',
    valueField: 'std_id',
    plugins: [],
    searchField: ['std_name','std_id'],
    dropdownDirection: 'down'
  };

  
  teacherConfig = {
    labelField: 'fullName',
    valueField: 'user_id',
    plugins: [],
    searchField: ['first_name','last_name','user_id'],
    dropdownDirection: 'down'
  };

  
  subjectConfig = {
    labelField: 'subject_name',
    valueField: 'subject_id',
    plugins: [],
    searchField: ['subject_name','subject_id'],
    dropdownDirection: 'down'
  };

  standardOptions: any = [];
  teacherOptions:any = [];
  subjectOptions:any = [];

  
  ngOnInit() {

    
    this.adminForm.getSubjects().subscribe(res=>{
      if(res.status == 200){
        this.subjectOptions = res.content;
      }
      else{
        //write dept failure fetch message
      }
    })

    this.adminForm.getStandards().subscribe(res=>{
      if(res.status == 200){
        this.standardOptions = res.content;
      }
      else{
        //write dept failure fetch message
      }
    })

    this.adminForm.getTeachers().subscribe(res=>{
      if(res.status == 200){
        console.log(res.content);
        this.teacherOptions = res.content;
      }
      else{
        //write dept failure fetch message
      }
    })

    this.classForm = this.fb.group({
      standard: ['',Validators.required],
      section:['',[Validators.required,Validators.maxLength(8)]],
      classTeacher: ['',Validators.required],
      academicYear: ['2018-2019',Validators.required],
      groups: this.fb.array([])
    })
    this.setGroups();
    // console.log(this.classForm['controls']['groups']['controls']);
    // console.log(this.classForm['controls']['groups']['controls'][0]['controls']['subjects']['controls'])
  }

  get formData() { return <FormArray>this.classForm.get('groups'); }


  previewSwitch(){
    console.log(this.classForm.value);
    this.previewFlag = !this.previewFlag;
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }

  getSubjectName(id){
   let index = this.subjectOptions.findIndex(sub=> sub.subject_id == id);
   return this.subjectOptions[index].subject_name;
  }

  getTeacherName(id){
    let index = this.teacherOptions.findIndex(teacher=> teacher.user_id == id);
    return this.teacherOptions[index].first_name + " " + this.teacherOptions[index].last_name;
   }

  getStandardName(id){
    let index = this.standardOptions.findIndex(standard=> standard.std_id == id);
    return this.standardOptions[index].std_name;
  }

  onSubmit() {

    this.adminForm.postClass(this.classForm.value).subscribe(res=>{
      console.log(res);
      if(res.status == 200){
        this.classForm.reset();
        this.message = "Successfully added the class in to our database :)"
      }
      else{
        this.message = "We are facing error in inserting your data REF:-" + res.message
      }
      this.previewFlag = false;
      this.messageFlag = true;
      setTimeout(function() {
        this.messageFlag = false;
    }.bind(this), 3000);
    })
  
  }


setGroups() {
  let control = <FormArray>this.classForm.controls.groups;
  this.class.groups.forEach(x =>{
      control.push(this.fb.group({
        groupName: x.groupName,
        subjects: this.setSubjects(x)
      }))
    })
  }

setSubjects(x){
  let arr = new FormArray([])
  x.subjects.forEach(y => {
    arr.push(this.fb.group({
      subject: [y.subject,Validators.required],
      teacher: [y.teacher,Validators.required]
    }))
  })
  return arr;
}

addNewGroup() {
  let control = <FormArray>this.classForm.controls.groups;
  console.log("clicked");
  control.push(
    this.fb.group({
      groupName: [''],
      subjects: this.fb.array([
        this.fb.group({
          subject: ['',Validators.required],
          teacher: ['',Validators.required]
        })
      ])
    })
  )
}

deleteGroup(index) {
  let control = <FormArray>this.classForm.controls.groups;
  console.log(index);
  if(index === 0){
    //group
  }
  else{
    control.removeAt(index)
  } 
}

deleteSubject(control) {
  if(control.length <= 1){

  }
  else{
    control.removeAt(control.length-1)
  } 
}

addOptions(control){
  console.log(control);
  control.push(
    this.fb.group({
      subject: ['',Validators.required],
      teacher: ['',Validators.required]
    }))
}

}
