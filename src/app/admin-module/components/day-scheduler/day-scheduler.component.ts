import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { AdminFormsService } from '../../../services/admin-forms.service';

@Component({
  selector: 'app-day-scheduler',
  templateUrl: './day-scheduler.component.html',
  styleUrls: ['./day-scheduler.component.scss']
})
export class DaySchedulerComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private adminForm: AdminFormsService) { }
  
  createScheduleForm:FormGroup;
  submitted:boolean = false;
  messagePanel:string;
  daysName:any[]=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
  standardConfig = {
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    labelField: 'std_name',
	  valueField: 'std_id',
  	maxItems: 24
  }

  sessionConfig = {
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    labelField: 'period_name',
	  valueField: 'period_id',
  	maxItems: 24
  }

  standardOptions:any[] = [];
  scheduleOptions:any[] = [];
  sessionOptions:any[] = [];
  datesInSchedules:any[] = [];

  myFilter = (d: Date): boolean => {
    return !(this.datesInSchedules.find(date=>{
      let scheduleDate = new Date(date.date);
      return ((d.getFullYear() == scheduleDate.getFullYear())
      &&(d.getMonth() == scheduleDate.getMonth())
      &&(d.getDate() == scheduleDate.getDate()));
     }))
    // Prevent Saturday and Sunday from being selected. [owlDateTimeFilter]="myFilter"
}

  ngOnInit() {

    this.adminForm.getStandards().subscribe(res=>{
      if(res.status == 200){
        this.standardOptions = res.content;
      }
      else{
        //write dept failure fetch message
      }
    })

    this.adminForm.getDatesInSchedules().subscribe(res=>{
      if(res.status == 200){
        this.datesInSchedules = res.content;
      }
      else{
        //write dept failure fetch message
      }
    })

    this.adminForm.getSessions().subscribe(res=>{
      if(res.status == 200){
        this.sessionOptions = res.content;
      }
    })
    this.adminForm.getSchedules().subscribe(res=>{
      if(res.status == 200){
        this.scheduleOptions = res.content;
      }
      else{
        //write dept failure fetch message
      }

    })

    this.createScheduleForm = this.formBuilder.group({
      dateRange: ['',[Validators.required]],
      dayRange: ['',[Validators.required]],
      scheduleType: [1,[Validators.required]],
      applicableTo:[0,[Validators.required]],
      swapDay:[],
      selectedStandards:[''],
      selectedSessions:[''],
      holidayType:['']
    });
  }

  get f() { return this.createScheduleForm.controls; }

  standardValidation(){
    if(this.f.applicableTo.value != 0){
      this.f.selectedStandards.setValidators([Validators.required]);
    }
    else{
      this.f.selectedStandards.clearValidators();
    }
  }

  scheduleChangeValidation(){
    if(this.f.scheduleType.value == 2){
      this.f.selectedSessions.setValidators([Validators.required]);
    }
    else if(this.f.scheduleType.value == 4){
      this.f.swapDay.setValidators([Validators.required]);
    }
    else if(this.f.scheduleType.value == 5){
      this.f.holidayType.setValidators([Validators.required]);
    }
    else{
      this.f.selectedSessions.clearValidators();
      this.f.swapDay.clearValidators();
      this.f.holidayType.clearValidators();
    }
  }

  submitFlag(){
    this.submitted = true;
  }

  dateChange(){
    let dateRanges:any[] = this.createScheduleForm.controls.dateRange.value;
    console.log(dateRanges[0].getDay());
    let day:any[]=[];
    dateRanges.forEach(date=>{
      day.push(this.daysName[date.getDay()]);
    })
    this.createScheduleForm.controls.dayRange.setValue(day.join("~"));
  }

  onSubmit() {
    
    // if(this.createScheduleForm.valid){
    //   console.log('reaDY TO trigger')
    // }
    this.adminForm.generateSchedules(this.createScheduleForm.value).subscribe(res=>{
      console.log(res);
    })
}

}
