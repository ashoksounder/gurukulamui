import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiscellaneousFormsComponent } from './miscellaneous-forms.component';

describe('MiscellaneousFormsComponent', () => {
  let component: MiscellaneousFormsComponent;
  let fixture: ComponentFixture<MiscellaneousFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiscellaneousFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiscellaneousFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
