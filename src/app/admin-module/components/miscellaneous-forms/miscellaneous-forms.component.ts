import { Component, OnInit } from '@angular/core';
import { AdminFormsService } from '../../../services/admin-forms.service'

@Component({
  selector: 'app-miscellaneous-forms',
  templateUrl: './miscellaneous-forms.component.html',
  styleUrls: ['./miscellaneous-forms.component.scss']
})
export class MiscellaneousFormsComponent implements OnInit {

  standards: any[] = [];
  standardTempValue:string;
  standardMessage:string ="";
  standardMessageFlag:boolean=false;
  getStandardFlag:boolean = false;

  departments: any[] = [];
  departmentTempValue:string;
  departmentMessage:string ="";
  departmentMessageFlag:boolean=false;
  getdepartmentFlag:boolean = false;

  constructor(private adminService: AdminFormsService) { }


  ngOnInit() {
    this.adminService.getStandards().subscribe(res=>{
      if(res.status == 200){
        this.standards = res.content;
      }
      else{
        this.standards = [];
        this.getStandardFlag = true;
        this.getdepartmentFlag = true;
      }
    })

    this.adminService.getDepartments().subscribe(res=>{
      if(res.status == 200){
        this.departments = res.content;
      }
      else{
        this.departments = [];
        this.getdepartmentFlag = true;
      }
    })
  }

  doEditDepartment(i){
    this.departmentTempValue=this.departments[i].dept_name;
    this.departments[i].editFlag = 1;
  }

  doSaveDepartment(i,dept_name){
    this.departments[i].spinnerFlag = 1;
    this.adminService.updateDepartments(dept_name,this.departments[i].dept_id ).subscribe(res=>{
      this.departments[i].spinnerFlag = 0;
      this.departments[i].editFlag = 0;
      if(res.status == 200){
        this.departmentMessageFlag=true;
        this.departmentMessage = res.message;
        setTimeout(function() { 
          this.departmentMessageFlag = false;
      }.bind(this), 2000);
      }
      else{
        this.departments[i].dept_name=this.standardTempValue;
        this.departmentMessageFlag=true;
        this.departmentMessage = res.message;
        setTimeout(function() {
          this.departmentMessageFlag = false;
      }.bind(this), 3000);
      }
    })
  }

  doAddDepartment(name){
    this.adminService.postDepartments(name).subscribe(res=>{
      if(res.status == 200){
        this.departmentMessageFlag=true;
        this.departmentMessage = res.message;
        this.departments.push({
          dept_id: res.content.insertId,
          dept_name: name
        })
        setTimeout(function() {
          this.departmentMessageFlag = false;
      }.bind(this), 3000);
      }
      else{
        this.departmentMessageFlag=true;
        this.departmentMessage = res.message;
        setTimeout(function() {
          this.departmentMessageFlag = false;
      }.bind(this), 3000);
      }
    })
  }

  
  doEditStandard(i){
    this.standardTempValue=this.standards[i].std_name;
    this.standards[i].editFlag = 1;
  }

  doSaveStandard(i, std_name){
    this.standards[i].spinnerFlag = 1;
    this.adminService.updateStandards(std_name,this.standards[i].std_id ).subscribe(res=>{
      this.standards[i].spinnerFlag = 0;
      this.standards[i].editFlag = 0;
      if(res.status == 200){
        this.standardMessageFlag=true;
        this.standardMessage = res.message;
        setTimeout(function() {
          this.standardMessageFlag = false;
      }.bind(this), 2000);
      }
      else{
        this.standards[i].std_name=this.standardTempValue;
        this.standardMessageFlag=true;
        this.standardMessage = res.message;
        setTimeout(function() {
          this.standardMessageFlag = false;
      }.bind(this), 3000);
      }
    })
  }

  doAddStandard(name){
    this.adminService.postStandards(name).subscribe(res=>{
      if(res.status == 200){
        this.standardMessageFlag=true;
        this.standardMessage = res.message;
        this.standards.push({
          std_id: res.content.insertId,
          std_name: name
        })
        setTimeout(function() {
          this.standardMessageFlag = false;
      }.bind(this), 3000);
      }
      else{
        this.standardMessageFlag=true;
        this.standardMessage = res.message;
        setTimeout(function() {
          this.standardMessageFlag = false;
      }.bind(this), 3000);
      }
    })
  }


}
