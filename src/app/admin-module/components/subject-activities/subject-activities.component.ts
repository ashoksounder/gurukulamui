import { Component, OnInit } from '@angular/core';
import { AdminFormsService } from '../../../services/admin-forms.service'

@Component({
  selector: 'app-subject-activities',
  templateUrl: './subject-activities.component.html',
  styleUrls: ['./subject-activities.component.scss']
})
export class SubjectActivitiesComponent implements OnInit {
  
  rawData: any[] = [];
  
  subjects: any[] = [];
  subjectTempValue:string;
  subjectMessage:string ="";
  subjectMessageFlag:boolean=false;
  getsubjectFlag:boolean = false;

  activities: any[] = [];
  activityTempValue:string;
  activityMessage:string ="";
  activityMessageFlag:boolean=false;
  getactivityFlag:boolean = false;

  constructor(private adminService: AdminFormsService) { }

  ngOnInit() {
    this.adminService.getSubjects().subscribe(res=>{
      if(res.status == 200){
        this.rawData = res.content;
        this.rawData.forEach(data=>{
          if(data.activity_flag){
            this.activities.push(data);
          }
          else{
            this.subjects.push(data);
          }
        })
      }
      else{
        this.rawData = [];
        this.getsubjectFlag = true;
        this.getactivityFlag = true;
      }
    })
  }

  doEditSubject(i){
    this.subjectTempValue=this.subjects[i].subject_name;
    this.subjects[i].editFlag = 1;
  }

  doSaveSubject(i,subject_name){
    this.subjects[i].spinnerFlag = 1;
    this.adminService.updateSubjects(subject_name,this.subjects[i].subject_id ).subscribe(res=>{
      this.subjects[i].spinnerFlag = 0;
      this.subjects[i].editFlag = 0;
      if(res.status == 200){
        this.subjectMessageFlag=true;
        this.subjectMessage = res.message;
        setTimeout(function() {
          this.subjectMessageFlag = false;
      }.bind(this), 3000);
      }
      else{
        this.subjects[i].subject_name=this.subjectTempValue;
        this.subjectMessageFlag=true;
        this.subjectMessage = res.message;
        setTimeout(function() {
          this.subjectMessageFlag = false;
      }.bind(this), 3000);
      }
    })
  }

  doAddSubject(name){
    let flag = 0 //if it is subject the activity flag will be 0
    this.adminService.postSubjects(name,flag).subscribe(res=>{
      if(res.status == 200){
        this.subjectMessageFlag=true;
        this.subjectMessage = res.message;
        this.subjects.push({
          subject_id: res.content.insertId,
          subject_name: name,
          activity_flag:0
          //add spinner flag
        })
        setTimeout(function() {
          this.subjectMessageFlag = false;
      }.bind(this), 3000);
      }
      else{
        this.subjectMessageFlag=true;
        this.subjectMessage = res.message;
        setTimeout(function() {
          this.subjectMessageFlag = false;
      }.bind(this), 3000);
      }
    })
  }

  doEditActivity(i){
    this.activityTempValue=this.activities[i].subject_name;
    this.activities[i].editFlag = 1;
  }

  doSaveActivity(i,subject_name){
    this.activities[i].spinnerFlag = 1;
    this.adminService.updateSubjects(subject_name,this.activities[i].subject_id ).subscribe(res=>{
      this.activities[i].spinnerFlag = 0;
      this.activities[i].editFlag = 0;
      if(res.status == 200){
        this.activityMessageFlag=true;
        this.activityMessage = res.message;
        setTimeout(function() {
          this.activityMessageFlag = false;
      }.bind(this), 3000);
      }
      else{
        this.activities[i].subject_name=this.subjectTempValue;
        this.activityMessageFlag=true;
        this.activityMessage = res.message;
        setTimeout(function() {
          this.activityMessageFlag = false;
      }.bind(this), 3000);
      }
    })
  }

  doAddActivity(name){
    let flag = 1 //if it is subject the activity flag will be 0
    this.adminService.postSubjects(name,flag).subscribe(res=>{
      if(res.status == 200){
        this.activityMessageFlag=true;
        this.activityMessage = res.message;
        this.activities.push({
          subject_id: res.content.insertId,
          subject_name: name,
          activity_flag:1
          //add spinner flag
        })
        setTimeout(function() {
          this.activityMessageFlag = false;
      }.bind(this), 3000);
      }
      else{
        this.activityMessageFlag=true;
        this.activityMessage = res.message;
        setTimeout(function() {
          this.activityMessageFlag = false;
      }.bind(this), 3000);
      }
    })
  }
}
