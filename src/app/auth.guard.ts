import { Injectable } from '@angular/core';
import { Router,ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UsersService } from './services/users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,public jwtHelper: JwtHelperService, private userService:UsersService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (sessionStorage.getItem('access_token') && !this.jwtHelper.isTokenExpired()) {

      let cipher = sessionStorage.getItem("session");
      let role:{} = this.userService.dataDecrypt(cipher)
              // check if route is restricted by role
          if (route.data.roles && route.data.roles.indexOf(role['role']) == -1) {
            console.log(route.data.roles);
            console.log(role);
              // role not authorised so redirect to home page
              this.router.navigate(['/']);
              return false;
          }
  
          // authorised so return true
          return true;

    }

    this.router.navigate(['login']);
    return false;
  }
  
}
