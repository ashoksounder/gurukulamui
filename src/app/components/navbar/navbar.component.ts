import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersService } from '../../services/users.service';
declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  data: any;
  role: number;
  constructor(private userService:UsersService) { }

  ngOnInit() {
    this.userService.userInfo().subscribe(res=>{
        if(res.content){
          this.data = res.content[0];
          console.log(this.data);
        }
    })
   this.role = this.userService.getRole();
  }

  toggle(){
    $('.sidebar-offcanvas').toggleClass('active');
  }

  logout(){
    this.userService.logout();
  }

}
