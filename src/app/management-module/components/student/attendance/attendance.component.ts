import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { ChartType } from 'chart.js';
import { SingleDataSet, Label } from 'ng2-charts';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ManagementService } from '../../../../services/management.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.scss']
})
export class AttendanceComponent implements OnInit {

  dataSource: any;
  elRef: ElementRef;
  rawData:any[]=[];
  sortedData:{};
  absFlag:any[];
  renderFlag:boolean = false;
  totalSessionCount:number;
  attendancePercentage:number;
  pendingSessions:any[] = [];
  attendedSessions:any[] = [];
  id:any;
  missedSessions:any[] = [];
  onDutySessions:any[] = [];
  otherSessions:any[] = [];
  displayedColumns: string[] = ['Date', 'Session', 'Schedule', 'Subject', 'Teacher', 'Status'];

  // Doughnut
  public doughnutChartLabels: Label[] = ['Attended', 'Missed','Pending', 'On-Duty'];
  public doughnutChartData: SingleDataSet;
  public doughnutChartType: ChartType = 'doughnut';

  constructor(private toastr: ToastrService, private route: ActivatedRoute, private managementService: ManagementService) {

  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
  
    this.id = this.route.snapshot.parent.paramMap.get('id');
    this.managementService.getAllSessionsforStudent(this.id).subscribe(res => {
      this.dataSource = new MatTableDataSource(res.content);
      this.rawData = res.content;
      //table
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      //graphs and headers
      this.sortedData = this.groupBy(this.rawData,"abs_flag")
      this.absFlag = Object.keys(this.sortedData);
      let tempSessions:any[] = [];
      this.absFlag.forEach(flag=>{
        if(flag == 1){
          this.missedSessions = this.sortedData[flag]
        }
        else if(flag == 2){
          this.onDutySessions = this.sortedData[flag]
        }
        else if(flag == "null"){
          tempSessions= this.sortedData[flag]
        }
        else{
          this.otherSessions = this.sortedData[flag]
        }
      })
      tempSessions.forEach(session=>{
        if(session.status_flag == null){
          this.pendingSessions.push(session);
        }
        else{
          this.attendedSessions.push(session);
        }
      })
      this.totalSessionCount = this.attendedSessions.length+this.missedSessions.length+this.onDutySessions.length+this.pendingSessions.length;
      this.attendancePercentage = Math.round((this.attendedSessions.length+this.onDutySessions.length)/(this.totalSessionCount-this.pendingSessions.length)*100);
      this.renderChart();
    })
  }

  renderChart() {
    let data:number[] = [this.attendedSessions.length,this.missedSessions.length,this.pendingSessions.length,this.onDutySessions.length];
    this.doughnutChartData = data;
    console.log(this.doughnutChartData);
    this.renderFlag = true;
  }

  groupBy(xs, key) {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

}
