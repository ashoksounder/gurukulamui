import { Component, OnInit } from '@angular/core';
import { ManagementService } from '../../../../services/management.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-examreports',
  templateUrl: './examreports.component.html',
  styleUrls: ['./examreports.component.scss']
})
export class ExamreportsComponent implements OnInit {

  reports:any[] = [];
  id:any;

  constructor(private managementService:ManagementService,private route: ActivatedRoute) { }

  ngOnInit() {

    this.id = this.route.snapshot.parent.paramMap.get('id');
    this.managementService.getPublishedReports(this.id).subscribe(res=>{
      if (res.status == 200) {
        this.reports = res.content;
      }
      else{
        //error flag
      }
    })
  }

}
