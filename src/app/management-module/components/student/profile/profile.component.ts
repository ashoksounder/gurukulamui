import { Component, OnInit } from '@angular/core';
import { ManagementService } from '../../../../services/management.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userData:{};
  teacherSubjects:any[];
  studentId:any; 

  constructor(private managementService:ManagementService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.studentId = this.route.parent.snapshot.paramMap.get('id');
    this.managementService.getProfileInfo(this.studentId).subscribe(res=>{
      this.userData = res.content[0];
    })

    this.managementService.getSubjectTeachersInfo(50).subscribe(res=>{
      this.teacherSubjects = res.content;
    })
  }

}
