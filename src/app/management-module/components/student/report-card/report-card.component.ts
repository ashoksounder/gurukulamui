import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Router, ActivatedRoute } from '@angular/router';
import { Label } from 'ng2-charts';
import { ManagementService} from '../../../../services/management.service';

@Component({
  selector: 'app-report-card',
  templateUrl: './report-card.component.html',
  styleUrls: ['./report-card.component.scss']
})
export class ReportCardComponent implements OnInit {

  
  examId: any;
  studentId: any;
  reportDB: any[] = [];
  selfInfo:{};
  myGroupReport: {};
  myGroup:any[] = [];
  classToppers:any[] = [];
  reportGroups:any[] = [];
  myTotal:string;
  myRank:any;
  myPercentage:any;
  renderChartFlag:boolean;
  topperFlag:boolean =false;
  failFlag:boolean = false;

  public barChartOptions: ChartOptions = {
    responsive: true
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'horizontalBar';
  public barChartLegend = true;
  public barChartPlugins = []; 


  public barChartData: ChartDataSets[] = [];
  public barChartData2: ChartDataSets[] = [];
  constructor(private managementService:ManagementService,  private route: ActivatedRoute) { }

  ngOnInit() {

    this.studentId = this.route.parent.snapshot.paramMap.get('id');
    this.examId = this.route.snapshot.paramMap.get('id2');
    this.renderChartFlag = false;

    //service to get info on us
    this.managementService.getStudentReportInfo(this.examId,this.studentId).subscribe(res=>{
      if (res.status == 200) {
        this.selfInfo = res.content[0];
      }
      else{
        //error flag
      }
    })
    // service to get individual report
    this.managementService.getStudentExamReport(this.examId,this.studentId).subscribe(res=>{
      if (res.status == 200) {
        this.reportDB = res.content;
        this.myGroupReport = this.groupBy(this.reportDB, 'report_group');
        this.myGroup = Object.keys(this.myGroupReport);
        this.renderData();
      }
      else{
        //error flag
      }
    })
    //service to get average and max marks in report
    this.managementService.getMaxAvgMarks(this.examId,this.studentId).subscribe(res=>{
      if (res.status == 200) {
        let highest:number[]=[];
        let labels:string[] = [];
        let average:number[]=[];
        let maxAvgData:any[] = res.content;
        maxAvgData.forEach(group=>{
          highest.push(parseInt(group.highest));
          average.push(parseInt(group.average));
          labels.push(group.group_name)
          this.reportGroups.push(group.report_group)
        });
        this.barChartLabels = labels;
        this.barChartData.push( { data: highest, label: 'Highest' })
        this.barChartData2.push( { data: average, label: 'Average' })
        this.renderData();
      }
      
    })
    // service to get overall report
    this.managementService.getClassToppersForReport(this.examId,this.studentId).subscribe(res=>{
      if (res.status == 200) {
        this.classToppers = res.content;
        this.topperFlag=true;
        this.renderData();
      }
      else{
        //error flag
      }
    })
  }

  renderData(){
    console.log("triggered");
    if(this.reportGroups.length>=1 && this.myGroup.length>=1 && this.topperFlag){
      let data:number[] =[];
      let totalMax = 0;
      let total = 0;
      let studentIndex: number;
      let studentFk:number;
      this.reportGroups.forEach(group=>{
        data.push(parseInt(this.myGroupReport[group][0].group_total));
        totalMax = totalMax + parseInt(this.myGroupReport[group][0].group_max);
        total = total+parseInt(this.myGroupReport[group][0].group_total)
        studentFk = this.myGroupReport[group][0].student_fk;

        //to check pass or fail to provide ranks
        if(this.myGroupReport[group][0].pass_flag === 0){
          this.failFlag = true;
        }
 
      })
      if(!this.failFlag){
        studentIndex = this.classToppers.findIndex(student=> student.student_fk === studentFk);
        this.myRank = this.classToppers[studentIndex].rank;
      }
      else{
        this.myRank = "-";
      }
      this.barChartData.push({ data: data, label: 'You'});
      this.barChartData2.push({ data: data, label: 'You'});
      this.myTotal = total + "/" + totalMax;
      this.myPercentage = Math.round((total/totalMax) * 100)
      this.renderChartFlag = true;
    }
  }

  groupBy(xs, key) {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

}
