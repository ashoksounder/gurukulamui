import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable  } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminFormsService {

  constructor(private http:HttpClient) { }

  serverapiURL = "http://localhost:3000/api/";

  getRoles():Observable<any>{
    return this.http.get(this.serverapiURL+"adminForms/roles").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getStandards():Observable<any>{
    return this.http.get(this.serverapiURL+"adminForms/standards").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
        console.log(response);
      return response;
      })); 
  }

  postStandards(name):Observable<any>{
    let data={
      std_name: name
    }
    return this.http.post(this.serverapiURL+"adminForms/standards",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  updateStandards(name,id):Observable<any>{
    let data={
      std_name: name
    }
    return this.http.patch(this.serverapiURL+"adminForms/standards/"+id,data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getDepartments():Observable<any>{
    return this.http.get(this.serverapiURL+"adminForms/departments").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
        console.log(response);
      return response;
      })); 
  }

  postDepartments(name):Observable<any>{
    let data={
      dept_name: name
    }
    return this.http.post(this.serverapiURL+"adminForms/departments",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  updateDepartments(name,id):Observable<any>{
    let data={
      dept_name: name
    }
    return this.http.patch(this.serverapiURL+"adminForms/departments/"+id,data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  } 

  getSubjects():Observable<any>{
    return this.http.get(this.serverapiURL+"adminForms/subjects").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  postSubjects(name,flag):Observable<any>{
    let data={
      subject_name: name,
      activity_flag: flag
    }
    console.log(data);
    return this.http.post(this.serverapiURL+"adminForms/subjects",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  updateSubjects(name,id):Observable<any>{
    let data={
      subject_name: name
    }
    return this.http.patch(this.serverapiURL+"adminForms/subjects/"+id,data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  postTeachers(data):Observable<any>{
    return this.http.post(this.serverapiURL+"adminForms/createteachers",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getSessions():Observable<any>{
    return this.http.get(this.serverapiURL+"adminForms/sessions").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }


  getTeachers():Observable<any>{
    return this.http.get(this.serverapiURL+"adminForms/teachers").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  postClass(data):Observable<any>{
    return this.http.post(this.serverapiURL+"adminForms/createclass",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getSchedules():Observable<any>{
    return this.http.get(this.serverapiURL+"adminForms/typeofschedules").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getDatesInSchedules():Observable<any>{
    return this.http.get(this.serverapiURL+"adminForms/datesinschedules").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  generateSchedules(data):Observable<any>{
    return this.http.post(this.serverapiURL+"adminForms/insertschedules",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
        console.log(response);
      return response;
      })); 
  }

  getExamNames():Observable<any>{
    return this.http.get(this.serverapiURL+"adminForms/gettypeofexams").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
        console.log(response);
      return response;
      })); 
  }

  getListofSections(id):Observable<any>{
    let data = {
      id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'adminForms/listofsections',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getListofSubjects(sectionList):Observable<any>{
    let data = {
      sections: sectionList
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'adminForms/listofsubjects',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  insertExamSchedules(data):Observable<any>{
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'adminForms/insertexamschedules',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  
}
