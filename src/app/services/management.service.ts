import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable  } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ManagementService {

  constructor(private http:HttpClient) { }

  serverapiURL = "http://localhost:3000/api/";

  getScannedStudents(searchKey,limit):Observable<any>{
    let data = {
      search_string: searchKey,
      limit_id: limit
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'management/studentscanner',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getAllSessionsforStudent(id):Observable<any>{
    let data = {
      id:id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'management/allsessions',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
   }

   getPublishedReports(id):Observable<any>{
     let data = {
       student_id:id
     }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'management/getstudentspublishedreports',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getStudentReportInfo(id1,id2):Observable<any>{
    let data = {
      exam_id: id1,
      student_id: id2
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'management/getreportinfo',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getStudentExamReport(id1,id2):Observable<any>{
    let data = {
      exam_id: id1,
      student_id: id2
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'management/studentexamreport',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getClassToppersForReport(id1,id2):Observable<any>{
    let data = {
      exam_id: id1,
      student_id: id2
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'management/getreportclasstoppers',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getMaxAvgMarks(id1,id2):Observable<any>{
    let data = {
      exam_id: id1,
      student_id: id2
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'management/myexamavgmaxreport',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getProfileInfo(id):Observable<any>{
    let data = {
      student_id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'management/studentprofileinfo',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getSubjectTeachersInfo(id):Observable<any>{
    let data = {
      student_id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'management/studentsubjectteachers',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

}
