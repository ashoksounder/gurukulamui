import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map,tap } from 'rxjs/operators';
import { Observable  } from 'rxjs';
import { RouterModule, Routes, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  serverapiURL = "http://localhost:3000/api/";
  constructor(private http:HttpClient, private router: Router) {
   }
   
   getAllSessions():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'students/allsessions').pipe(map(res=>{
      console.log(res);
      return res;
    }))
   }

   getListofDays():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'students/listofdays').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getSchedule():Observable<any>{
    return this.http.get(this.serverapiURL+"students/getschedule").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getExamSchedule():Observable<any>{
    return this.http.get(this.serverapiURL+"students/getexamschedule").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getClassToppers(id1):Observable<any>{
    let data = {
      exam_id: id1
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'students/getclasstoppers',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getMyReportInfo(id1):Observable<any>{
    let data = {
      exam_id: id1
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'students/getreportinfo',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getMyExamReport(id1):Observable<any>{
    let data = {
      exam_id: id1
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'students/myexamreport',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getMaxAvgMarks(id1):Observable<any>{
    let data = {
      exam_id: id1
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'students/myexamavgmaxreport',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getPublishedReports():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'students/getpublishedreports').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

}
