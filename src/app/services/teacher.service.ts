import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map,tap } from 'rxjs/operators';
import { Observable  } from 'rxjs';
import { RouterModule, Routes, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http:HttpClient, private router: Router) { }

  serverapiURL = "http://localhost:3000/api/";

  getMyClassList():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/classlist').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getMyClass(id):Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/myclass/' + id).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  checkUserName(value):Observable<any>{
    return this.http.post(this.serverapiURL+"teachers/username",{"userName":value}).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  addNewStudent(data):Observable<any>{
    return this.http.post(this.serverapiURL+"teachers/addnewstudent",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getListofDays():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/listofdays').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getListofPeriods():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/listofperiods').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getGroupsofClass(id):Observable<any>{

    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/classgroups/' + id).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getListofStudents(id):Observable<any>{
    let data = {
      id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/listofstudents',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  addSchedule(data):Observable<any>{
    return this.http.post(this.serverapiURL+"teachers/addschedule",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getSchedule(data):Observable<any>{
    return this.http.post(this.serverapiURL+"teachers/getschedule",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getTodayTeacherSchedule():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/teachertodayschedule').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getSessionInfo(id):Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/sessioninfo/' + id).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getSessionAction():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/sessionactionlist').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getSessionStudents(sessionId):Observable<any>{
    return this.http.post(this.serverapiURL+"teachers/sessionstudents",{id:sessionId}).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  postSessionData(formData):Observable<any>{
    return this.http.post(this.serverapiURL+"teachers/sessiondatafeed",formData).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getSessionAbsentees(id):Observable<any>{
    let data = {
      id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/getsessionabsentees',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getSwapInfo(id):Observable<any>{
    let data = {
      id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/getswapinfo',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getSwapApprovals():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/swapapprovalsopen').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getAllIndividualSwap():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/swapdetailed').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  swapUpdate(data):Observable<any>{
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/updateswap',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  cancelAbortRequest(id):Observable<any>{
    let data = {
      id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/cancelabort',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getSessionCountonClass(id):Observable<any>{
    let data = {
      id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/countofclassperiods',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getAllSchedules():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/teacherallschedules').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getTeacherExamReports():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/teacherexamreports').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getDetailedExamReports(id):Observable<any>{
    let data = {
      id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/getdetailedexamreport',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }
  
  postExamMarks(data):Observable<any>{
    return this.http.post(this.serverapiURL+"teachers/postexammarks",data).pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  getPendingExamConf():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/getexamconfig').pipe(map(res=>{
      console.log(res);
      return res;
      })); 
  }

  getExamConfDetail(id1,id2):Observable<any>{
    let data = {
      class_id :id1,
      exam_id: id2
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/examconfdetail',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getExamSchedule():Observable<any>{
    return this.http.get(this.serverapiURL+"teachers/getexamschedule").pipe(
      map((response:{status:Number, content: any[],message:string })=>{
      return response;
      })); 
  }

  postExamConfiguration(data):Observable<any>{
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/postexamconf',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getExamReport(id1,id2):Observable<any>{
    let data = {
      class_id :id1,
      exam_id: id2
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/classexamreport',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getDepartmentWiseTeacher():Observable<any>{
    return this.http.get<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/departmentwiseteachers').pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }

  getProfileInfo(id):Observable<any>{
    let data = {
      teacher_id: id
    }
    return this.http.post<{status:Number,message:string ,content:  [{}]}>(this.serverapiURL+ 'teachers/teacherprofileinfo',data).pipe(map(res=>{
      console.log(res);
      return res;
    }))
  }
  // getDateFromISO(isodate){
  //   let date = new Date(isodate);
  //   let year = date.getFullYear();
  //   let month = date.getMonth()+1;
  //   let dt = date.getDate();

  //   if (dt < 10) {
  //     dt = dt.toString()
  //     dt = '0' + dt;
  //   }
  //   if (month < 10) {
  //     month = '0' + month;
  //   }

  //   console.log(year+'-' + month + '-'+dt);
  // }

}
