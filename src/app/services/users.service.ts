import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { RouterModule, Routes, Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  secretKey: string = "mindyour@#business";
  plaintext: {};
  constructor(private http: HttpClient, private router: Router) { }

  serverapiURL = "http://localhost:3000/api/";

  //data encrypt for local storage
  dataEncrypt(data: {}) {
    var cipher:string = CryptoJS.AES.encrypt(JSON.stringify(data), this.secretKey);
    return cipher;
  }

  //data decrypt from local storage
  dataDecrypt(data) {
    var bytes = CryptoJS.AES.decrypt(data, this.secretKey);
    this.plaintext = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    return this.plaintext;
  }

  login(userName: string, password: string) {
    return this.http.post<{ status: Number, message: string,content: [{}],access_token: string }>(this.serverapiURL + "login", { userName, password }).pipe(tap(res => {
      if (res.status == 200) {
        sessionStorage.setItem('access_token', res.access_token);
        sessionStorage.setItem('session',this.dataEncrypt(res.content[0]))
      }
      return res;
    }))
  }

  logout() {
    sessionStorage.removeItem('access_token');
    sessionStorage.removeItem('session');
    this.router.navigate(['login']);
  }

  getRole(){
    let role = this.dataDecrypt(sessionStorage.getItem("session"));
    return role['role'];
  }

  userInfo() {
    return this.http.get<{ status: Number, message: string, content: [{}] }>(this.serverapiURL + 'users/info').pipe(map(res => {
      let userData = res.content[0];
      return res;
    }))
  }

  public get loggedIn(): boolean {
    return (sessionStorage.getItem('access_token') !== null);
  }

}
