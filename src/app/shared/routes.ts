import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { HomeComponent } from '../components/home/home.component';
import { LoginComponent } from '../components/login/login.component';
import { AuthGuard } from '../auth.guard';

export const routes: Routes = [
  { path: 'app', component: NavbarComponent ,canActivate:[AuthGuard], children: [
    { path: 'admin', loadChildren: () => import('../admin-module/admin-module.module').then(mod => mod.AdminModuleModule)},
    { path: 'teacher', loadChildren: () => import('../teacher-module/teacher-module.module').then(mod => mod.TeacherModuleModule)},
    { path: 'student', loadChildren: () => import('../student-module/student-module.module').then(mod => mod.StudentModuleModule)},
    { path: 'management', loadChildren: () => import('../management-module/management-module.module').then(mod => mod.ManagementModuleModule)},
    // { path: 'createClass', component: CreateClassComponent,canActivate:[AuthGuard]},
    { path: 'dashboard', component:HomeComponent,canActivate:[AuthGuard] },
    { path: '', redirectTo:'dashboard',pathMatch:'full' }
  ]},
  { path: 'login', component:LoginComponent },
  { path: '', redirectTo:'login',pathMatch:'full' }
]

 