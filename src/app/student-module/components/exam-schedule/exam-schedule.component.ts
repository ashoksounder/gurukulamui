import { Component, OnInit } from '@angular/core';
import { StudentService} from '../../../services/student.service';

@Component({
  selector: 'app-exam-schedule',
  templateUrl: './exam-schedule.component.html',
  styleUrls: ['./exam-schedule.component.scss']
})
export class ExamScheduleComponent implements OnInit {

  allExamSchedules: any[];
  sortedExamSchedules: any;
  upcomingExamSchedules: any[] = [];
  ongoingExamSchedules:any[] = [];
  completedExamSchedules:any[] = [];
  exams: any[] = [];
  today = new Date();
  constructor(private studentService:StudentService) { }

  ngOnInit() {
    this.studentService.getExamSchedule().subscribe(res=>{
      this.allExamSchedules = res.content;
      this.sortedExamSchedules = this.groupBy(this.allExamSchedules,'exam_id');
      console.log(this.sortedExamSchedules);
      for(var k in this.sortedExamSchedules){
        this.exams.push(k)
      }
      this.exams.forEach(exam=>{
        let lastIndex = this.sortedExamSchedules[exam].length - 1;
        console.log(this.sortedExamSchedules[exam][0].date);
        console.log(this.today.toDateString());
        if((new Date(this.sortedExamSchedules[exam][0].date)).getDate() <= this.today.getDate() && (new Date(this.sortedExamSchedules[exam][lastIndex].date)).getDate() >= this.today.getDate()){
          this.ongoingExamSchedules.push(this.sortedExamSchedules[exam]);
        }
        else if(new Date(this.sortedExamSchedules[exam][0].date).getDate() >= this.today.getDate()){
          this.upcomingExamSchedules.push(this.sortedExamSchedules[exam]);
        }
        else{
          this.completedExamSchedules.push(this.sortedExamSchedules[exam]);
        }
      })
      console.log('ongoingExamSchedules',this.ongoingExamSchedules);
      console.log('upcomingExamSchedules',this.upcomingExamSchedules);
      console.log('completedExamSchedules',this.completedExamSchedules);
  })
}

  groupBy(xs, key) {
    return xs.reduce((rv, x)=> {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };


  getDaysRemaining(date){
      const diffTime = Math.abs (this.today.getDate() - new Date(date).getDate());
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
      return diffDays;
      }
}
