import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schedule-panel',
  templateUrl: './schedule-panel.component.html',
  styleUrls: ['./schedule-panel.component.scss']
})
export class SchedulePanelComponent implements OnInit {

  listOfPeriods: any[] = ['1st Session','1st Session','1st Session','1st Session','1st Session','1st Session','1st Session','1st Session'];
  colorCodes:any[] = ["green","turquoise","navy","blue","purple","grey","red","orange","yellow","lime-green","saffron"]
  constructor() { }

  ngOnInit() {
  }

}
