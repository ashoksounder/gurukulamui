import { Component, OnInit } from '@angular/core';
import { StudentService} from '../../../services/student.service';

@Component({
  selector: 'app-weekly-schedules',
  templateUrl: './weekly-schedules.component.html',
  styleUrls: ['./weekly-schedules.component.scss']
})
export class WeeklySchedulesComponent implements OnInit {

  constructor(private studentService:StudentService) { }

  submitted:boolean = false;
  classId: string;
  messagePanel:string;
  periodSplit:[{}] =[{}];
  listOfDays: [{}] = [{}];
  listOfPeriods: any[] = [];
  groupData:[{}] = [{}];
  groupOptions:[{}] = [{}];
  listOfGroups:[{}] =[{}];
  classSchedules:any[]=[];
  groupedSubjectInfo:[{}] = [{}];
  colorCodes:any[] = ["green","turquoise","navy","blue","purple","grey","red","orange","yellow","lime-green","saffron"]
  periodConfig = {
    labelField: 'period_name',
    valueField: 'period_id',
    plugins: [],
    searchField: ['period_id','period_name'],
    dropdownDirection: 'down'
  };
  dayConfig = {
    labelField: 'day_name',
    valueField: 'day_id',
    plugins: [],
    searchField: ['day_id','day_name'],
    dropdownDirection: 'down'
  };
  groupConfig = {
    labelField: 'group_name',
    valueField: 'group_id',
    plugins: [],
    searchField: ['group_name','group_id'],
    dropdownDirection: 'down'
  };

  ngOnInit() {

    //service to get days
    this.studentService.getListofDays().subscribe(res=>{
      this.listOfDays = res.content;
      // console.log(this.listOfDays);
    });
  
    this.studentService.getSchedule().subscribe(res=>{
      let data = res.content;
      this.classSchedules = this.groupBy(data,'day_fk');
    })

  }

  groupBy(xs, key) {
    return xs.reduce((rv, x)=> {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  }
