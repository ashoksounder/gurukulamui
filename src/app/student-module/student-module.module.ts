import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from '../auth.guard';
import { Ng2OdometerModule } from 'ng2-odometer';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { ChartsModule } from 'ng2-charts';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SummaryPanelComponent } from './components/summary-panel/summary-panel.component';
import { SchedulePanelComponent } from './components/schedule-panel/schedule-panel.component';
import { AllSessionsComponent } from './components/all-sessions/all-sessions.component';
import { WeeklySchedulesComponent } from './components/weekly-schedules/weekly-schedules.component';
import { ExamScheduleComponent } from './components/exam-schedule/exam-schedule.component';
import { ExamReportsComponent } from './components/exam-reports/exam-reports.component';
import { TestReportsComponent } from './components/test-reports/test-reports.component';
import { StudentReportCardComponent } from './components/student-report-card/student-report-card.component';



const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], data: { roles: [4] } },
  { path: 'allsessions', component: AllSessionsComponent, canActivate: [AuthGuard], data: { roles: [4] } },
  { path: 'weeklyschedules', component: WeeklySchedulesComponent, canActivate: [AuthGuard], data: { roles: [4] } },
  { path: 'examschedules', component: ExamScheduleComponent, canActivate: [AuthGuard], data: { roles: [4] } },
  { path: 'examreport', component: ExamReportsComponent, canActivate: [AuthGuard], data: { roles: [4] } },
  { path: 'testreport', component: TestReportsComponent, canActivate: [AuthGuard], data: { roles: [4] } },
  { path: 'studentreport/:id', component: StudentReportCardComponent,canActivate:[AuthGuard], data: {roles: [4]}},
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  declarations: [DashboardComponent, SummaryPanelComponent, SchedulePanelComponent, AllSessionsComponent, WeeklySchedulesComponent, ExamScheduleComponent, ExamReportsComponent, TestReportsComponent, StudentReportCardComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    MatSortModule,
    ChartsModule,
    MatInputModule,
    MatPaginatorModule,
    Ng2OdometerModule.forRoot(),
    RouterModule.forChild(routes)
  ]
})
export class StudentModuleModule {

}
