import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigDetailedComponent } from './config-detailed.component';

describe('ConfigDetailedComponent', () => {
  let component: ConfigDetailedComponent;
  let fixture: ComponentFixture<ConfigDetailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigDetailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
