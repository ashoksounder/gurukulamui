import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators, FormArray } from '@angular/forms';
import { TeacherService} from '../../../services/teacher.service';
import { Router, ActivatedRoute } from '@angular/router';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-create-students',
  templateUrl: './create-students.component.html',
  styleUrls: ['./create-students.component.scss']
})
export class CreateStudentsComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private teacherService:TeacherService, private route: ActivatedRoute) { }


  createStudentForm:FormGroup;
  submitted:boolean = false;
  emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  phonePattern = /^[6789]\d{9}$/;
  userNamePattern =/^[a-z][a-z0-9]{6,15}$/;
  passwordPattern = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/
  departmentOptions: any = [];
  subjectInfo:[{}] = [{}];
  groupedSubjectInfo:[{}]=[{}];
  messagePanel:string;
  messageFlag:boolean = false;
  groups:any[];
  userNameFlag:boolean;
  classId: string;
  subjectFlag: boolean = false;
  selectConfig = {
    labelField: 'dept_name',
    valueField: 'dept_id',
    plugins: [],
    searchField: ['dept_name', 'dept_id'],
    dropdownDirection: 'down'
  };
  selectedSubjects = {};
  errorFlag:boolean = false;

  ngOnInit() {

    this.classId = this.route.snapshot.paramMap.get('id');
    this.teacherService.getMyClass(this.classId).subscribe(res=>{
      if(res.status == 200){
        this.subjectInfo = res.content;
        this.groupedSubjectInfo =   this.groupBy(this.subjectInfo,'group_id');
        this.groups= Object.keys(this.groupedSubjectInfo);
        this.groups.forEach(key=>{
          this.selectedSubjects[key]=this.groupedSubjectInfo[key][0].subject_id;
        })
      }
    })

    this.createStudentForm = this.formBuilder.group({
      firstName: ['',[Validators.required,Validators.minLength(3)]],
      lastName: ['',[Validators.required,Validators.minLength(3)]], 
      gender: [0,Validators.required],
      dob: ['',Validators.required],
      joindate:['', Validators.required],
      mail:['',[Validators.required, Validators.pattern(this.emailPattern)]],
      password:['',[Validators.required, Validators.pattern(this.passwordPattern)]],
      userName:['',[Validators.required, Validators.pattern(this.userNamePattern)]],
      primaryContact:['',[Validators.required, Validators.pattern(this.phonePattern)]],
      secondaryContact:['',[Validators.required, Validators.pattern(this.phonePattern)]],
      address1:['',[Validators.required,Validators.minLength(3)]],
      address2:['',[Validators.required,Validators.minLength(3)]],
      city:['',[Validators.required,Validators.minLength(3)]],
      postcode:['',Validators.required],
      state:['',[Validators.required,Validators.minLength(3)]]
    });
  }

  get f() { return this.createStudentForm.controls; }

  userNameCheck(){
    console.log(this.createStudentForm.controls.userName.value);
    let uName:string = this.createStudentForm.controls.userName.value;
    if(uName.length>5){
      this.teacherService.checkUserName(uName).subscribe(res=>{
        let check:[] = res.content;
        if(check.length>=1){
          this.userNameFlag = true;
        }
        else{
          this.userNameFlag = false;
        }
      })
    }
  }

  groupBy(xs, key) {
    return xs.reduce((rv, x)=> {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  log(){
    console.log(this.f.dob.value);
  }

  submitFlag(){
    this.submitted = true;
  }

  flagChanger(){
    this.submitted = true;
    if(this.createStudentForm.valid && !this.userNameFlag){
      this.subjectFlag = !this.subjectFlag;
    }
  }

  onSubmit() {

   if(this.createStudentForm.valid){
    let subjects =[];
    this.groups.forEach(key=>{
      subjects.push(this.selectedSubjects[key])
    })
    let data= this.createStudentForm.value;
    data.subjects = subjects;
    data.class_id =this.classId;

    this.teacherService.addNewStudent(data).subscribe(res=>{
      if(res.status == 200){

      }
      else{
        this.errorFlag = true;
      }
      this.messagePanel = res.message;
      this.messageFlag = true;
      setTimeout(function() {
      this.messageFlag = false;
      this.errorFlag = false;
      }.bind(this), 4000);
    })

    this.createStudentForm.reset();
    this.submitted = false;
    this.subjectFlag = false;
   }
   else{
    this.messagePanel = "Please fill the form with valid information";
   }
}

}
