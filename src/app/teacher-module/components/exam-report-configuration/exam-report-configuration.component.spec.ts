import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamReportConfigurationComponent } from './exam-report-configuration.component';

describe('ExamReportConfigurationComponent', () => {
  let component: ExamReportConfigurationComponent;
  let fixture: ComponentFixture<ExamReportConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamReportConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamReportConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
