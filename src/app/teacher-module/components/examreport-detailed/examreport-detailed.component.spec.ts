import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamreportDetailedComponent } from './examreport-detailed.component';

describe('ExamreportDetailedComponent', () => {
  let component: ExamreportDetailedComponent;
  let fixture: ComponentFixture<ExamreportDetailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamreportDetailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamreportDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
