import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,FormArray, Validators } from '@angular/forms';
import { TeacherService } from '../../../services/teacher.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-examreport-detailed',
  templateUrl: './examreport-detailed.component.html',
  styleUrls: ['./examreport-detailed.component.scss']
})
export class ExamreportDetailedComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private toastr: ToastrService,private route: ActivatedRoute, private teacherService: TeacherService) { }

  createReportForm:FormGroup;
  submitted:boolean = false;
  id: string;
  messagePanel:string;
  duplicateFlag:boolean = false;
  renderFlag:boolean = false;
  noDataFlag:boolean = false;
  loaderFlag:boolean= true;
  lockEditingFlag:boolean = false;
  studentList:any[] = [];

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get('id');
    this.teacherService.getDetailedExamReports(this.id).subscribe(res=>{
      this.studentList = res.content;
      if(this.studentList.length>=1){
        this.createReportForm = this.formBuilder.group({
          className : [this.studentList[0].std_name + "-" + this.studentList[0].section_name,[Validators.required]],
          subjectName: [this.studentList[0].subject_name,[Validators.required]],
          examName: [this.studentList[0].exam_name,[Validators.required]],
          studentInfo: this.formBuilder.array([])
        });
        this.setStudentInfo();
        this.loaderFlag = false;
        this.renderFlag = true;
      }
      else{
        this.loaderFlag = false;
        this.noDataFlag = true;
      }
    })
  }

  get f() { return this.createReportForm.controls; }

  get studentf(){ return this.createReportForm.controls.studentInfo["controls"]; }

  setStudentInfo() {
    let control = <FormArray>this.createReportForm.controls.studentInfo;
    this.studentList.forEach(x =>{
        control.push(this.formBuilder.group({
          studentName:[x.fullName, Validators.required],
          studentId:[x.user_id, Validators.required],
          maxMarks:[x.max_marks, Validators.required],
          absentFlag:[{value:x.abs_flag==null?0:1,disabled: true}],
          marksObtained:[{value:x.mark_scored,disabled: x.abs_flag==null?false:true}, x.abs_flag==null?Validators.required:""],
          comments:[{value:x.comments,disabled: x.abs_flag==null?false:true},  x.abs_flag==null?Validators.required:""],
        }))
      });
    }
  
    onSubmit(){
      this.submitted = true;
      let data =  this.createReportForm.value;
      let studentInfo:any[] = [];
      let postedData: any = {};
      if(data.studentInfo.length >= 1){
        data.studentInfo.forEach(info => {
          studentInfo.push([info.studentId,this.id,info.marksObtained,info.comments,null]);
        });
        postedData.id = this.id;
        postedData.studentInfo = studentInfo;
        this.teacherService.postExamMarks(postedData).subscribe(res=>{
          console.log(res);
        })
      }
      
      console.log(data);
    }

}
