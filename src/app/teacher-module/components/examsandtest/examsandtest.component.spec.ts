import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsandtestComponent } from './examsandtest.component';

describe('ExamsandtestComponent', () => {
  let component: ExamsandtestComponent;
  let fixture: ComponentFixture<ExamsandtestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsandtestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsandtestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
