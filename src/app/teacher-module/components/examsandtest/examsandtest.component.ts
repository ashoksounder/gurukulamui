import { Component, OnInit } from '@angular/core';
import { TeacherService} from '../../../services/teacher.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-examsandtest',
  templateUrl: './examsandtest.component.html',
  styleUrls: ['./examsandtest.component.scss']
})
export class ExamsandtestComponent implements OnInit {

  constructor(private teacherService:TeacherService,private router:Router) { }
  allReports:any[] = [];
  pendingReports:any[] = [];
  closedReports:any[] = [];

  ngOnInit() {
    this.teacherService.getTeacherExamReports().subscribe(res=>{
      this.allReports = res.content;
      this.allReports.forEach(report=>{
        if(report.status_flag == null){
          this.pendingReports.push(report);
        }
        else{
          this.closedReports.push(report);
        }
      })
      console.log(this.allReports);
    })
  }

}
