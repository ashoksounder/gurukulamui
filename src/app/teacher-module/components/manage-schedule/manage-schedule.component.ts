import { Component, OnInit } from '@angular/core';
import { TeacherService} from '../../../services/teacher.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manage-schedule',
  templateUrl: './manage-schedule.component.html',
  styleUrls: ['./manage-schedule.component.scss']
})
export class ManageScheduleComponent implements OnInit {

  classInfo:[{}] = [{}];
  sessions:any[] = [];
  day = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
  sessionLoader: boolean = false;
  constructor(private teacherService:TeacherService,private router:Router) { }

  ngOnInit() {
    this.teacherService.getMyClassList().subscribe(res=>{
      this.classInfo = res.content;
      this.classInfo.forEach((section,index)=>{
       this.getSessionList(section['class_id'],index);
      })
    })
  }

  getSessionList(class_id,index:number){
    let data =[];
    this.teacherService.getSessionCountonClass(class_id).subscribe(res=>{
      data = res.content;
      this.sessions[index] = data;
      if(index === this.classInfo.length-1){
        this.sessionLoader = true
      }
      });
  }

}
