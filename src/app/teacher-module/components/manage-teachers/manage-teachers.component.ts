import { Component, OnInit } from '@angular/core';
import { TeacherService} from '../../../services/teacher.service';
import { FilterPipe} from '../../../pipes/filter.pipe';

@Component({
  selector: 'app-manage-teachers',
  templateUrl: './manage-teachers.component.html',
  styleUrls: ['./manage-teachers.component.scss']
})
export class ManageTeachersComponent implements OnInit {

  teacherList:any[];
  deptName:string;
  searchText:string = "";
  constructor(private teacherService:TeacherService) { }

  ngOnInit() {
    this.teacherService.getDepartmentWiseTeacher().subscribe(res=>{
      this.teacherList = res.content;
      this.deptName = res.content[0].dept_name;
    })
  }

}
