import { Component, OnInit } from '@angular/core';
import { TeacherService} from '../../../services/teacher.service';

@Component({
  selector: 'app-myapprovals',
  templateUrl: './myapprovals.component.html',
  styleUrls: ['./myapprovals.component.scss']
})
export class MyapprovalsComponent implements OnInit {

  swapRequests: any[] = [];
  constructor(private teacherService:TeacherService) { }

  ngOnInit() {
    this.teacherService.getSwapApprovals().subscribe(res=>{
      // console.log('approvals',res.content)
      this.swapRequests = res.content;
    })
  }

}
