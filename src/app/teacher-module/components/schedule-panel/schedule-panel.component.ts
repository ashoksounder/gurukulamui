import { Component, OnInit } from '@angular/core';
import { TeacherService} from '../../../services/teacher.service';

@Component({
  selector: 'app-schedule-panel',
  templateUrl: './schedule-panel.component.html',
  styleUrls: ['./schedule-panel.component.scss']
})
export class SchedulePanelComponent implements OnInit {

  schedulesList: any[] = [];
  schedulesToday: any[] = [];
  schedulesTommorow: any[] = [];
  schedulesYesterday: any[] = [];
  messagePanel:string = "";
  errorFlag: boolean = false;
  interval: any[] = [];
  // this gives an object with dates as keys


  constructor(private teacherService: TeacherService) { }

  ngOnInit() {
    this.teacherService.getTodayTeacherSchedule().subscribe(res=>{
      if(res.status == 200){
        this.interval = res.content.interval;
        this.schedulesList = res.content.schedules;
        this.schedulesList.forEach(schedule=>{
          if(schedule.status_flag != 9){
            if(schedule.date == this.interval[0]){
              this.schedulesYesterday.push(schedule);
            }
            else if(schedule.date == this.interval[1]){
              this.schedulesToday.push(schedule);
            }
            else if(schedule.date == this.interval[2]){
              this.schedulesTommorow.push(schedule);
            }
            else{
            
            }
          }

        })
      }

      else{
        this.errorFlag = true;
        this.messagePanel = res.message + ":-Error on data fetching"
      }
    })
  }



}
