import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwapTeachersComponent } from './swap-teachers.component';

describe('SwapTeachersComponent', () => {
  let component: SwapTeachersComponent;
  let fixture: ComponentFixture<SwapTeachersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwapTeachersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwapTeachersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
