import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwapdetailedComponent } from './swapdetailed.component';

describe('SwapdetailedComponent', () => {
  let component: SwapdetailedComponent;
  let fixture: ComponentFixture<SwapdetailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwapdetailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwapdetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
