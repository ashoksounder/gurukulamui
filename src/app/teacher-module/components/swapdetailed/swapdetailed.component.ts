import { Component, OnInit } from '@angular/core';
import { TeacherService} from '../../../services/teacher.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-swapdetailed',
  templateUrl: './swapdetailed.component.html',
  styleUrls: ['./swapdetailed.component.scss']
})
export class SwapdetailedComponent implements OnInit {

  swapRequests: any[] = [];
  openRequests: any[] = [];
  closedRequests: any[] = [];
  constructor(private teacherService:TeacherService,private toastr: ToastrService) { }

  ngOnInit() {
    this.teacherService.getAllIndividualSwap().subscribe(res=>{
      this.swapRequests = res.content;
      this.swapRequests.forEach(res=>{
        if(res.swap_status_fk === 1){
          res.likeMouseFlag = false;
          res.disLikeMouseFlag = false;
          this.openRequests.push(res);
        }
        else{
          this.closedRequests.push(res);
        }
      })
    })
  }

  approveSwap(index){
     this.openRequests[index].swap_status_fk = 2;
     let data = {
       session_id : this.openRequests[index].session_fk,
       swap_id:  this.openRequests[index].swap_id,
       swap_status: 2,
       status_flag: null,
       group_id: this.openRequests[index].swap_to,
       date: this.openRequests[index].date,
       class_id: this.openRequests[index].class_id,
       period_id: this.openRequests[index].period_id
     }
     this.closedRequests.unshift(this.openRequests.splice(index,1)[0]);
     this.teacherService.swapUpdate(data).subscribe(res=>{
      if(res.status == 200){
        this.toastr.info('Hoorayy all set!',res.message);
      }
      else{
        this.toastr.warning('Contact Admin:-',res.message);
        this.openRequests.unshift(this.closedRequests.splice(0,1)[0])
      }
     })
  }

  rejectSwap(index){
    this.openRequests[index].swap_status_fk = 2;
    let data = {
      session_id : this.openRequests[index].session_fk,
      swap_id:  this.openRequests[index].swap_id,
      swap_status: 3,
      status_flag: null
    }
    this.closedRequests.unshift(this.openRequests.splice(index,1)[0]);
    this.teacherService.swapUpdate(data).subscribe(res=>{
     if(res.status == 200){
       this.toastr.info('Hoorayy all set!',res.message);
     }
     else{
       this.toastr.warning('Contact Admin:-',res.message);
       this.openRequests.unshift(this.closedRequests.splice(0,1)[0])
     }
    })
  }

}
