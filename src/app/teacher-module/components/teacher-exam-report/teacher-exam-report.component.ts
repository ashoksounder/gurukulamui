import { Component, OnInit, ViewChild, ElementRef,Inject } from '@angular/core';
import { TeacherService } from '../../../services/teacher.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as jsPDF from 'jspdf'
import html2canvas from 'html2canvas';
(window as any).html2canvas = html2canvas;

@Component({
  selector: 'app-teacher-exam-report',
  templateUrl: './teacher-exam-report.component.html',
  styleUrls: ['./teacher-exam-report.component.scss'],
  providers: [
    { provide: 'Window',  useValue: window }
  ]
})

//yet to add a service to get header title about exam and class info and download work pending
export class TeacherExamReportComponent implements OnInit {

  dataSource:any;
  elRef: ElementRef;

  constructor(private toastr: ToastrService,@Inject('Window') private window: Window, elRef: ElementRef, private route: ActivatedRoute, private teacherService: TeacherService) {
    this.elRef = elRef;
  }

  classId: any;
  examId: any;
  displayedColumns: string[];
  descriptionColumns: string[];
  reportDB: any[] = [];
  studentSortedReport: {};
  groupSortedReport: {};
  subjectSortedReport: {};
  students: any[] = [];
  studentTableData: any[] = [];
  groupTableData: any[] = [];
  totalMarks:number=0;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.classId = this.route.snapshot.paramMap.get('id');
    this.examId = this.route.snapshot.paramMap.get('id2');

    
    this.teacherService.getExamReport(this.classId, this.examId).subscribe(res => {
      if (res.status == 200) {
        this.reportDB = res.content;
        this.studentSortedReport = this.groupBy(this.reportDB, 'student_fk');
        this.groupSortedReport = this.groupBy(this.reportDB, 'report_group');
        this.subjectSortedReport = this.groupBy(this.reportDB, 'subject_fk');
        this.students = Object.keys(this.studentSortedReport);
        let reportGroup;
        this.students.forEach(student => {
          reportGroup = [];
          let studentData = {};
          studentData["total"] = this.studentSortedReport[student][0].total;
          studentData["rank"] = this.studentSortedReport[student][0].rank;
          studentData["fullname"] = this.studentSortedReport[student][0].fullname;
          studentData["failcounter"] = this.studentSortedReport[student][0].failcounter;
          studentData["status"] = this.studentSortedReport[student][0].failcounter == 0 ? true : false;
          studentData["rank"] = this.studentSortedReport[student][0].rank == null ? "NA" : this.studentSortedReport[student][0].rank;
          this.studentSortedReport[student].forEach(data => {
            if (!reportGroup.includes(data.report_group)) {
              studentData[data.group_name] = data.group_total;
              reportGroup.push(data.report_group);
            }
          })
          this.studentTableData.push(studentData);
        })
        const sorted = this.studentTableData.sort((a, b) => a.fullname.localeCompare(b.fullname));
        this.dataSource = new MatTableDataSource(sorted);
        this.dataSource.sort = this.sort;
        // this.sortData({ active: "fullname", direction: "asc" });
        this.displayedColumns = ['Index', 'Name', 'Total', 'Result', 'Rank'];
        this.descriptionColumns = ['#','#','TotalMarks_description','#','#'];

        reportGroup.forEach(group => {
          let data = {}
          let stringData = "";
          data["groupId"] = group;
          data["groupName"] = this.groupSortedReport[group][0].group_name;
          data["passMarks"] = this.groupSortedReport[group][0].pass_marks;
          data["maxMarks"] = this.groupSortedReport[group][0].group_max;
          this.totalMarks=this.totalMarks+data["maxMarks"];
          stringData = data["groupName"] + "_description"
          this.displayedColumns.splice(this.displayedColumns.length - 3, 0, data["groupName"])
          this.descriptionColumns.splice(this.descriptionColumns.length - 3,0,stringData)
          this.groupTableData.push(data);
        })
      }
      else {
        //error
      }
    });
  }

  groupBy(xs, key) {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  downloadPDF() {
    
// var margin = {
//   top: 0,
//   left: 0,
//   right: 0,
//   bottom: 0
// };
//     html2canvas(document.getElementById("foo")).then(canvas => {

//       var pdf = new jsPDF('l', 'pt', [1502, 768]);

//       var imgData  = canvas.toDataURL("image/jpeg", 1.0);
//       pdf.addImage(imgData,0,0,canvas.width, canvas.height);
//       pdf.save('converteddoc.pdf');
//   });
const html = document.getElementById("foo");
const pdf = new jsPDF('landscape', 'px', 'a4');

pdf.fromHTML(html, 0, 0, {
    'width': html.clientWidth,
    'height': html.clientHeight
}, (a) => { pdf.save('web.pdf'); });
  }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

